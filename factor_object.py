import sqlite3
import logging

class FactorObject():
    def __init__(self, factor_name, factor_data, database='factors.db', log_file_name = 'app.log', logging_level = logging.INFO):
        try:
            self.factor_name = factor_name
            self.factor_data = factor_data
            self.database = database
            logging.basicConfig(filename=log_file_name, filemode='w', format='%(name)s - %(levelname)s - %(message)s', level=logging_level)
            logging.info('Initalized task to create factor %s', self.factor_name)
            self.create_factor_table()
        except Exception as ex:
            logging.error('Error initalizing task %s:  %s',  self.factor_name, str(ex))


    def create_factor_table(self):
        try:
            conn = sqlite3.connect(self.database)
            sql = 'CREATE TABLE ' + self.factor_name + ' AS ' + self.factor_data
            cursor = conn.execute(sql)
            results = cursor.fetchall()
            conn.commit()
            conn.close()
            logging.info('%s, commited', results)
        except Exception as ex:
            logging.error('Error retreiveing data for Factor %s:  %s',  self.factor_name, str(ex))


#if __name__ == '__main__':
#    FactorObject(factor_name = 'CLAIM_DENSITY', factor_data = "select 'Bakersfield', 19.48 UNION select 'Oildale', 11.22")
